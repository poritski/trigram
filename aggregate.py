# coding: utf-8

# Imports

from TrigramShared import *

# Constants

AGGREGATED_MODEL = 'full'
SOURCE_LIST = ['etext%02d' % i for i in range(6) + range(90, 100)]

# Main loop

if __name__ == '__main__':
    word_frequency = dd(lambda: 0)  # word => total frequency
    word_to_id_old = dd(lambda: dd(lambda: 0))  # word => source dir => id
    word_to_id_new = dd(lambda: 0)  # word => remapped id
    id_remapping = dd(lambda: dd(lambda: 0))  # source dir => id => remapped id
    trigrams = dd(lambda: 0)  # trigram code => frequency

    print 'Aggregating the following trigram models:'
    print ' '.join(SOURCE_LIST)
    start_time = time()

    print 'Reading unigram data...'
    for source in SOURCE_LIST:
        with open('./data/words-' + source + '.csv', mode='rb') as f:
            csv_reader = csv.reader(f)
            for row in csv_reader:
                w = row[0].decode(TEXT_ENCODING)
                word_frequency[w] += int(row[1])
                word_to_id_old[w][source] = int(row[2])

    print 'Remapping word IDs...'
    descending_freq = sorted(word_frequency, key=lambda w: word_frequency[w],
                             reverse=True)
    for (i, w) in enumerate(descending_freq):
        word_to_id_new[w] = i+1
        for source in word_to_id_old[w]:
            id_remapping[source][word_to_id_old[w][source]] = i+1
    word_to_id_new['@NONE@'] = 0
    for source in word_to_id_old['@NONE@']:
        id_remapping[source][word_to_id_old['@NONE@'][source]] = 0

    for source in SOURCE_LIST:
        print 'Reading trigram data from', source, '...'
        with open('./data/trigrams-' + source + '.csv', mode='rb') as f:
            csv_reader = csv.reader(f)
            for row in csv_reader:
                trigram_id, trigram_freq = int(row[0]), int(row[1])
                a, b, c = [id_remapping[source][x]
                           for x in integer_decode(trigram_id)]
                trigrams[integer_encode(a, b, c)] += trigram_freq

    # Output the aggregated trigram model
    print 'Writing the output...'
    with open('./data/words-' + AGGREGATED_MODEL + '.csv', mode='wb') as f:
        csv_writer = csv.writer(f)
        for w in word_frequency:
            row = [w.encode(TEXT_ENCODING), word_frequency[w],
                   word_to_id_new[w]]
            csv_writer.writerow(row)

    with open('./data/trigrams-' + AGGREGATED_MODEL + '.csv', mode='wb') as f:
        csv_writer = csv.writer(f)
        for row in trigrams.iteritems():
            csv_writer.writerow(row)

    # Performance statistics
    print 'Complete in %.2f seconds.' % (time()-start_time)
    print 'The corpus contains', sum(word_frequency.values()), 'tokens.'
    print 'There are', len(word_frequency), 'types (distinct tokens) and',\
          len(trigrams), 'distinct trigrams.'
