# coding: utf-8

# Imports

from TrigramShared import *
from random import randint, shuffle

# Constants

DELIMITERS = [("`", "'"), ("``", "''"), ('(', ')'),
              ('[', ']'), ('{', '}'), ('<', '>')]
NO_SPACE_BEFORE = [x[1] for x in DELIMITERS] + ['.', '!', '?', ',', ';', ':']
NO_SPACE_AFTER = [x[0] for x in DELIMITERS]

MIN_SENTENCE_LEN = 3  # measured in words
MAX_PARAGRAPH_LEN = 15  # measured in sentences
START_WITH_CAPITALS = True
REMOVE_UNBALANCED_DELIMITERS = True

SOURCE = argv[1]
SENTENCE_COUNT = int(argv[2])

# Subroutines


def arg_min_difference(pairlist, target_value):
    # It's possible to obtain better performance via binary search
    min_difference = target_value
    best_candidate = pairlist[0][0]
    for p in pairlist:
        if abs(p[1] - target_value) < min_difference:
            min_difference = abs(p[1] - target_value)
            best_candidate = p[0]
    return best_candidate


def next_word(model, a, b):
    cumulative_freqs = model[integer_encode(a, b)]
    total_count = cumulative_freqs[-1][1]
    random_value = randint(0, total_count+1)
    return arg_min_difference(cumulative_freqs, random_value)


def is_balanced(sentence, delimiter_pair):
    # A quick-and-dirty solution. Checking a condition which is necessary
    # but insufficient: the first opening delimiter appears before the first
    # closing one, and the number of opening and closing delimiters is equal.
    opening, closing = 0, 0
    seen_before = False
    for x in sentence:
        if x == delimiter_pair[0]:
            if not seen_before:
                seen_before = True
            opening += 1
        elif x == delimiter_pair[1]:
            if not seen_before:
                return False
            else:
                closing += 1
    return opening == closing


def make_sentence(model, mapping):
    seq = [0, 0]
    while True:
        seq.append(next_word(model, seq[-2], seq[-1]))
        if seq[-2] == 0 and seq[-1] == 0:
            break
    s = [mapping[x] for x in seq[2:-2]]
    if REMOVE_UNBALANCED_DELIMITERS:
        # "'" is occasionally used as a self-delimiting symbol. Rather than
        # to balance it, we'd rather throw out all instances of it.
        s = [x for x in s if x != "'"]
        for pair in DELIMITERS:
            if not is_balanced(s, pair):
                s = [x for x in s if x not in pair]
    return s

# In the following three lambdas (in fact, they _were_ lambdas before a PEP8
# run) we don't check that type(x) == str, since in the context of their
# usage the argument cannot be of any other type.


def is_ucfirst(x):
    return x[0].isupper() and x.capitalize() == x


def is_apostrophed(x):
    return len(x) > 1 and x[0] == "'" and (x in ["n't", "N'T"] or
                                           x[1].isalpha() and x[1].islower())


def is_hyphened(x):
    return len(x) > 1 and x[-1] == '-' and x[-2].isalpha() and x[-2].islower()


def pretty_print(sentence):
    joined = ' '
    for w in sentence:
        if w in NO_SPACE_BEFORE or is_apostrophed(w):
            joined = joined[:-1]
        if is_hyphened(w):
            joined += w[:-1]
        elif w in NO_SPACE_AFTER:
            joined += w
        else:
            joined += (w + ' ')
    if joined[0] == ' ':
        joined = joined[1:]
    if joined[-1] == ' ':
        joined = joined[:-1]
    return joined

# Main loop

if __name__ == '__main__':
    word_frequency = dd(lambda: 0)  # word => frequency
    id_to_word = dd(lambda: '@NONE@')  # id => word
    next_word_freq = dd(lambda: [])  # bigram code => [(word id, freq), ...]

    print 'Trying to generate a text of approximately', SENTENCE_COUNT,\
          'sentences.'
    print 'Trigram model:', SOURCE
    start_time = time()

    # Load the requested model
    print 'Reading unigram data...'
    with open('./data/words-' + SOURCE + '.csv', mode='rb') as f:
        csv_reader = csv.reader(f)
        for row in csv_reader:
            w = row[0].decode(TEXT_ENCODING)
            word_frequency[w] = int(row[1])
            id_to_word[int(row[2])] = w

    print 'Reading trigram data...'
    with open('./data/trigrams-' + SOURCE + '.csv', mode='rb') as f:
        csv_reader = csv.reader(f)
        for row in csv_reader:
            trigram_id, trigram_freq = int(row[0]), int(row[1])
            a, b, c = integer_decode(trigram_id)
            initial_bigram_id = integer_encode(a, b)
            if START_WITH_CAPITALS and initial_bigram_id == 0\
               and not is_ucfirst(id_to_word[c]):
                continue
            else:
                if next_word_freq[initial_bigram_id]:
                    trigram_freq += next_word_freq[initial_bigram_id][-1][1]
                next_word_freq[initial_bigram_id].append((c, trigram_freq))

    # Generate text
    print 'Generating the text...'
    output = ''
    s_counter, w_counter = 0, 0
    while s_counter < SENTENCE_COUNT:
        # The number of sentences in the generated text will be no less than
        # SENTENCE_COUNT and no more than SENTENCE_COUNT+MAX_PARAGRAPH_LEN-1.
        target_len = randint(1, MAX_PARAGRAPH_LEN)
        current_paragraph = []
        while len(current_paragraph) < target_len:
            s = make_sentence(next_word_freq, id_to_word)
            if len(s) >= MIN_SENTENCE_LEN:
                current_paragraph.append(pretty_print(s))
                w_counter += len(s)
        output += (' '.join(current_paragraph) + '\n\n')
        s_counter += target_len

    # Output the generated text and performance statistics
    with open('./data/generated-'+SOURCE+'-'+str(SENTENCE_COUNT)+'.txt',
              mode='wb') as f:
        f.write(output.encode(TEXT_ENCODING))
    print 'Complete in %.2f seconds.' % (time()-start_time)
    print 'The generated text contains', s_counter, 'sentences and',\
          w_counter, 'tokens.'
