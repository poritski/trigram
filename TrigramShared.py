# coding: utf-8

# Imports

from sys import argv
from os import listdir
from collections import defaultdict as dd
from time import time
import csv

# Constants

TEXT_ENCODING = 'latin-1' # safe to use with Project Gutenberg texts
MARGIN = 10**6

# Subroutines

def integer_encode(*args):
    for x in args:
        assert type(x) == int and x < MARGIN
    raw = [x for x in args]
    max_power = len(raw)-1
    return sum([x * MARGIN**(max_power-i) for (i, x) in enumerate(raw)])

def integer_decode(x, expected_count=3):
    assert type(x) in (int, long)
    out = []
    for i in xrange(expected_count):
        out.append(x % MARGIN)
        x /= MARGIN
    out.reverse()
    return tuple(out)
