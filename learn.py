# coding: utf-8

# Imports

from TrigramShared import *
from nltk.tokenize import sent_tokenize, word_tokenize

# Constants

# The following lists of exclusions have been crafted manually from
# Project Gutenberg CD documentation (master_list.csv). Non-English texts
# are those with Language specified; additionally, rnpz810.txt is written
# in Polish, which is not indicated in the metadata. Anonymous texts are
# those with Author-LN equal to "Anonymous".
EXCL_NON_ENGLISH = [
    'anidl10.txt', 'clprm10u.txt', 'galli10.txt', '8cand10.txt', 'crtcm10.txt',
    '8clel10.txt', 'cdbfr10.txt', '8lndp10.txt', '2donq10.txt', '8swan11.txt',
    'vazov10.txt', 'handi10.txt', 'kalev10.txt', 'kknta10.txt', '0ddc809a.txt',
    '8lssm10u.txt', '8gs3410.txt', '7mynr10.txt', '8rdsl10.txt', '8va0910.txt',
    'rnpz810.txt']
EXCL_ANONYMOUS = [
    'bwulf11.txt', 'cndrl10.txt', '8dubc10.txt', 'niebl10.txt', 'nblng10.txt',
    'njals10.txt', 'ee710.txt', 'onepi10.txt', 'pimil10.txt', 'sorol10.txt',
    'vlsng10.txt', '8vnmm10.txt']
EXCLUSIONS = EXCL_NON_ENGLISH + EXCL_ANONYMOUS

INPUT_DIR = argv[1]
INPUT_DIR_PATH = '../' + INPUT_DIR
FILES = [x for x in listdir(INPUT_DIR_PATH)
         if x not in EXCLUSIONS and x[-4:] == '.txt']

# Subroutines


def remove_preamble(text):
    parts = text.split('START OF THE PROJECT GUTENBERG EBOOK')
    if len(parts) > 1:
        parts = parts[1].split('END OF THE PROJECT GUTENBERG EBOOK')
        return parts[0]
    else:
        parts = text.split('*END*')
        return parts[-1]

# Main loop

if __name__ == '__main__':
    word_frequency = dd(lambda: 0)  # word => frequency
    word_to_id = dd(lambda: 0)  # word => id (non-shared rank)
    tokenized_text = dd(lambda: [])  # filename => [[token, token, ...], ...]
    trigrams = dd(lambda: 0)  # trigram code => frequency

    print 'Learning a trigram model from %s...' % INPUT_DIR
    start_time = time()

    # Read files one by one
    for filename in FILES:
        # Extract raw file contents
        with open(INPUT_DIR_PATH + '/' + filename, mode='rb') as f:
            file_contents = remove_preamble(f.read().decode(TEXT_ENCODING))
        # Tokenize and update single-word frequencies
        tokenized_text[filename] = [word_tokenize(s)
                                    for s in sent_tokenize(file_contents)]
        for s in tokenized_text[filename]:
            for w in s:
                word_frequency[w] += 1

    # Encode words with their ranks by decreasing frequency
    # (rank 0 is assigned to the special token '@NONE@')
    descending_freq = sorted(word_frequency, key=lambda w: word_frequency[w],
                             reverse=True)
    for (i, w) in enumerate(descending_freq):
        word_to_id[w] = i+1
    word_to_id['@NONE@'] = 0

    # Compute trigram frequencies, including initial and final trigrams
    # (@NONE@ -- @NONE@ -- word and the like)
    for filename in tokenized_text:
        for s in tokenized_text[filename]:
            ids = [0, 0] + [word_to_id[s[j]] for j in xrange(len(s))] + [0, 0]
            for i in xrange(len(ids)-2):
                trigrams[integer_encode(ids[i], ids[i+1], ids[i+2])] += 1
    del tokenized_text

    # Output the trigram model learned so far
    with open('./data/words-' + INPUT_DIR + '.csv', mode='wb') as f:
        csv_writer = csv.writer(f)
        for w in word_frequency:
            row = [w.encode(TEXT_ENCODING), word_frequency[w], word_to_id[w]]
            csv_writer.writerow(row)

    with open('./data/trigrams-' + INPUT_DIR + '.csv', mode='wb') as f:
        csv_writer = csv.writer(f)
        for row in trigrams.iteritems():
            csv_writer.writerow(row)

    # Performance statistics
    print INPUT_DIR, 'complete in %.2f seconds (single-core time).' %\
                     (time()-start_time)
